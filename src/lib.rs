//! # Minimap2-Rust
//!
//! This is a tiny wrapper library to call [minimap2](https://github.com/lh3/minimap2) from Rust language.
//! This library uses Rust-bindgen as backend to generate bindings for minimap2 *on-the-fly.*
//! It creates bindings for the archtecture on which the compiler runs, rather than a static library for a specific arch.
//!
//! In the following documentation, the original documentation in the minimap2 header file would be cited if needed.
//!
//! To use this library, first install minimap2 object file(libminimap2.a in most case) to the location where `ld` could find.
//! Please make sure that ${LD_LIBRARY_PATH} is the location looked at in *running time*, not *compile time*, thus, it should be necessary to tell `cargo` where the library is by envirnmental variables like `RUSTFLAGS='-L /to/the/path'`.

#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
extern crate libc;
include!(concat!(env!("OUT_DIR"), "/bindings.rs"));
use std::ffi::CString;
/// An Index option to be used by minimap2.
#[derive(Debug)]
pub struct IndexOption {
    pub inner: mm_idxopt_t,
}

/// An Index of Minimap2 for DNA sequences.
#[derive(Debug)]
pub struct SeqIndex {
    pub inner: mm_idx_seq_t,
}

/// An Index of Minimap2. This includes the SeqIndex insede.
#[derive(Debug)]
pub struct Index {
    inner: *mut mm_idx_t,
}

impl Index {
    /// Create an index from specified DNA sequences.
    ///
    /// The indexing parameters should be given by a reference to IndexOption.
    /// The i-th sequence in `seq` has the name `name[i]`.
    /// Because the conversion from nomarl Rust strings to C-like string is
    /// indeed costly, it is the user to prepare sequence names with the type `&[CString]`
    /// # Example
    pub fn from_option(seq: &[&[u8]], name: &[CString], idx_op: &IndexOption) -> Index {
        let w = idx_op.inner.w as i32;
        let k = idx_op.inner.k as i32;
        let is_hpc = 1;
        let bucket_bits = idx_op.inner.bucket_bits as i32;
        let len = seq.len() as i32;
        let mut seq: Vec<*const i8> = seq.iter().map(|&e| e.as_ptr() as *const i8).collect();
        let seq_ptr = seq.as_mut_ptr();
        let mut name: Vec<_> = name.iter().map(|name| name.as_ptr()).collect();
        let inner =
            unsafe { mm_idx_str(w, k, is_hpc, bucket_bits, len, seq_ptr, name.as_mut_ptr()) };
        Index { inner }
    }
    /// Citation from minimap2.h the number of sequences is determined by rust.
    /// > Create an index from strings in memory
    /// > @param w            minimizer window size
    /// > @param k            minimizer k-mer size
    /// > @param is_hpc       use HPC k-mer if true
    /// > @param bucket_bits  number of bits for the first level of the hash table
    /// > @param n            number of sequences
    /// > @param seq          sequences in A/C/G/T
    /// > @param name         sequence names; could be NULL
    /// > @return minimap2 index
    pub fn from_settings(
        w: i32,
        k: i32,
        is_hpc: bool,
        bucket_bits: i32,
        seq: &[CString],
        name: &[CString],
    ) -> Index {
        let mut seq: Vec<_> = seq.iter().map(|e| e.as_ptr()).collect();
        let mut name: Vec<_> = name.iter().map(|e| e.as_ptr()).collect();
        let len = seq.len() as i32;
        let is_hpc = if is_hpc { 1 } else { 0 };
        let inner = unsafe {
            mm_idx_str(
                w,
                k,
                is_hpc,
                bucket_bits,
                len,
                seq.as_mut_ptr(),
                name.as_mut_ptr(),
            )
        };
        Index { inner }
    }
    pub fn print_info(&self) {
        unsafe { mm_idx_stat(self.inner) }
    }
    /// Updating mapping option by Index file.
    /// Citation from minimap2.h
    /// > Update mm_mapopt_t::mid_occ via mm_mapopt_t::mid_occ_frac
    /// > If mm_mapopt_t::mid_occ is 0, this function sets it to a number such that no
    /// > more than mm_mapopt_t::mid_occ_frac of minimizers in the index have a higher
    /// > occurrence.
    pub fn update_mapping_option(&self, mapopt: &mut MappingOption) {
        unsafe {
            mm_mapopt_update(&mut mapopt.inner, self.inner);
        }
    }

    /// Citation from minimap2.h
    /// > Align a query sequence against an index
    /// > This function possibly finds multiple alignments of the query sequence.
    /// > The returned array and the mm_reg1_t::p field of each element are allocated
    /// >  with malloc().
    /// > @param mi         minimap2 index
    /// > @param l_seq      length of the query sequence
    /// > @param seq        the query sequence
    /// > @param n_regs     number of hits (out)
    /// > @param b          thread-local buffer; two mm_map() calls shall not use one buffer at the same time!
    /// > @param opt        mapping parameters
    /// > @param name       query name, used for all-vs-all overlapping and debugging
    /// > @return an array of hits which need to be deallocated with free() together
    /// > with mm_reg1_t::p of each element. The size is written to _n_regs_.
    pub fn map(
        &self,
        query: &[i8],
        buf: &mut MemoryBuffer,
        opt: &MappingOption,
        name: &str,
    ) -> Alignments {
        let mut number_of_aln = 0;
        let l_seq = query.len() as i32;
        let name = CString::new(name).unwrap().as_ptr();
        let (inner, len) = unsafe {
            let res = mm_map(
                self.inner,
                l_seq,
                query.as_ptr(),
                &mut number_of_aln,
                buf.inner,
                &opt.inner,
                name,
            );
            let number_of_aln = number_of_aln as usize;
            (res, number_of_aln)
        };
        Alignments { inner, len }
    }
    /// Minimap2 citation:
    /// > Align a fasta/fastq file and print alignments to stdout
    pub fn map_file(&self, file: &str, opt: &MappingOption, n_threads: usize) {
        let file = CString::new(file).unwrap().as_ptr();
        let n_threads = n_threads as std::os::raw::c_int;
        unsafe {
            mm_map_file(self.inner, file, &opt.inner, n_threads);
        }
    }
    /// Name to id
    pub fn get_id_by_name(&self, name: &str) -> std::os::raw::c_int {
        let name = CString::new(name).unwrap().as_ptr();
        unsafe { mm_idx_name2id(self.inner, name) }
    }
    /// Rereference ID to Reference name
    pub fn get_name_by_id(&self, id: usize) -> Result<&str, std::str::Utf8Error> {
        let seq =
            unsafe { std::slice::from_raw_parts((*self.inner).seq, (*self.inner).n_seq as usize) };
        let seq = seq[id];
        unsafe { std::ffi::CStr::from_ptr(seq.name).to_str() }
    }
}

impl Drop for Index {
    fn drop(&mut self) {
        unsafe { mm_idx_destroy(self.inner) };
    }
}

/// Cigar
#[derive(PartialEq, Eq, Debug, Clone, Copy, Hash)]
pub enum Cigar {
    Match(u32),    // M
    Ins(u32),      // I
    Del(u32),      // D
    RefSkip(u32),  // N
    SoftClip(u32), // S
    HardClip(u32), // H
    Pad(u32),      // P
    Equal(u32),    // =
    Diff(u32),     // X
}

impl Cigar {
    pub fn new(op: u8, num: u32) -> Self {
        use Cigar::*;
        match op {
            b'M' => Match(num),
            b'I' => Ins(num),
            b'D' => Del(num),
            b'N' => RefSkip(num),
            b'S' => SoftClip(num),
            b'H' => HardClip(num),
            b'P' => Pad(num),
            b'=' => Equal(num),
            b'X' => Diff(num),
            _ => panic!(),
        }
    }
}

/// A record for minimap2 alignment
pub type Alignment = mm_reg1_t;

impl Alignment {
    pub fn score(&self) -> i32 {
        self.score
    }
    pub fn rid(&self) -> usize {
        self.rid as usize
    }
    pub fn query_start(&self) -> usize {
        self.qs as usize
    }
    pub fn query_end(&self) -> usize {
        self.qe as usize
    }
    pub fn reference_start(&self) -> usize {
        self.rs as usize
    }
    pub fn reference_end(&self) -> usize {
        self.re as usize
    }
    pub fn mapping_quality(&self) -> u32 {
        self.mapq()
    }
    pub fn is_rev(&self) -> bool {
        if self.rev() == 0 {
            false
        } else {
            true
        }
    }
    // for (i = 0; i < r->p->n_cigar; ++i) // IMPORTANT: this gives the CIGAR in the aligned regions. NO soft/hard clippings!
    pub fn cigar(&self) -> Option<Vec<Cigar>> {
        let cigar = unsafe {
            if (*self.p).cigar.as_ptr().is_null() {
                return None;
            } else {
                (*self.p).cigar.as_slice((*self.p).n_cigar as usize)
            }
        };
        let cigar: Vec<_> = cigar
            .iter()
            .map(|x| {
                let num = x >> 4;
                let op = b"MIDNSH"[(x & 0xf) as usize];
                Cigar::new(op, num)
            })
            .collect();
        Some(cigar)
    }
}

#[derive(Debug)]
pub struct Alignments {
    inner: *mut mm_reg1_t,
    len: usize,
}

impl Alignments {
    pub fn len(&self) -> usize {
        self.len
    }
    pub fn iter<'a>(&'a self) -> std::slice::Iter<'a, Alignment> {
        unsafe { std::slice::from_raw_parts(self.inner, self.len).iter() }
    }
    pub fn as_slice<'a>(&'a self) -> &'a [Alignment] {
        unsafe { std::slice::from_raw_parts(self.inner, self.len) }
    }
}

impl Drop for Alignments {
    fn drop(&mut self) {
        for aln in self.iter() {
            unsafe { libc::free(aln.p as *mut libc::c_void) };
        }
        unsafe { libc::free(self.inner as *mut libc::c_void) };
    }
}

/// A Mapping option to be used by minimp2.
#[derive(Debug)]
pub struct MappingOption {
    pub inner: mm_mapopt_t,
}

impl MappingOption{
    pub fn cigar_on(&mut self, is_on:bool){
        if is_on {
            self.inner.flag |= MM_F_CIGAR as i64;
        }else{
            self.inner.flag &= !(MM_F_CIGAR as i64);
        };
    }
    pub fn no_diag(&mut self, no_diag:bool){
        if no_diag {
            self.inner.flag |= MM_F_NO_DIAG as i64;
        }else{
            self.inner.flag &= !(MM_F_NO_DIAG as i64);
        };
        
    }
}

/// A reader struct to be used by Minimap2.
pub struct Reader {
    inner: *mut mm_idx_reader_t,
}

impl Drop for Reader {
    fn drop(&mut self) {
        unsafe { mm_idx_reader_close(self.inner) };
        std::mem::forget(self.inner);
    }
}

impl Reader {
    /// Initialize an index reader.
    /// if out_file is Some(file_name), the built index would be write down to that file.
    pub fn initialize(file: &str, opt: &IndexOption, out_file: Option<&str>) -> Self {
        let out_file = out_file.and_then(|e| CString::new(e).ok());
        let out_file = match out_file {
            Some(res) => res.as_ptr(),
            None => std::ptr::null(),
        };
        let in_file = CString::new(file).unwrap();
        let inner = unsafe { mm_idx_reader_open(in_file.as_ptr(), &opt.inner, out_file) };
        Reader { inner }
    }
    /// Read/build an index
    /// Read a given file(either index file or sequence file), build an index, and,
    /// if any, write the index to specified file.
    /// Return Some(index) if the reading sucessed. Otherwise, return None.
    /// Catation from minimap2.h
    /// > If the input file is an index file, this function reads one part of the
    /// > index and returns. If the input file is a sequence file (fasta or fastq),
    /// > this function constructs the index for about mm_idxopt_t::batch_size bases.
    /// > Importantly, for a huge collection of sequences, this function may only
    /// >  return an index for part of sequences. It needs to be repeatedly called
    /// > to traverse the entire index/sequence file.
    pub fn read_partial(&mut self, n_threads: i32) -> Option<Index> {
        unsafe {
            let inner = mm_idx_reader_read(self.inner, n_threads);
            if inner.is_null() {
                None
            } else {
                Some(Index { inner: inner })
            }
        }
    }
    /// See whether or not the reader has read through all the input sequece/index.
    pub fn has_finished(&self) -> bool {
        unsafe {
            match mm_idx_reader_eof(self.inner) {
                0 => false,
                _ => true,
            }
        }
    }
}

/// Memory buffer for thread-local storage during mapping
#[derive(Debug)]
pub struct MemoryBuffer {
    pub inner: *mut mm_tbuf_t,
}

impl MemoryBuffer {
    pub fn new() -> Self {
        let inner = unsafe { mm_tbuf_init() };
        MemoryBuffer { inner }
    }
}

impl Drop for MemoryBuffer {
    fn drop(&mut self) {
        unsafe { mm_tbuf_destroy(self.inner) }
        std::mem::forget(self.inner);
    }
}

/// Modify verbose level.
/// Citation from minimap2.h
/// > verbose level: 0 for no info, 1 for error, 2 for warning, 3 for message (default); debugging flag
pub fn set_verbose_level(level: i32) {
    unsafe {
        mm_verbose = level;
    }
}
/// Set debug level. If true, debug would be ON.
pub fn set_debug_level(is_debug: bool) {
    unsafe {
        if is_debug {
            mm_dbg_flag = 1
        } else {
            mm_dbg_flag = 0
        }
    }
}

/// The presets. `map` meanas mapping reads to the reference, `ava` means all-vs-all alignments.
/// `ont` is abbreviations for `OxfordNanoporeTechnology`, while `pb` is short for `PacBio`.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Preset {
    MapPB,
    MapONT,
    AvAPB,
    AvAONT,
    ASM5,
    ASM10,
    ASM20,
    Splice,
    None,
}

impl Preset {
    fn to_cstr(&self) -> CString {
        let preset = match *self {
            Preset::ASM10 => "asm10",
            Preset::ASM20 => "asm20",
            Preset::ASM5 => "asm5",
            Preset::AvAONT => "ava-ont",
            Preset::AvAPB => "ava-pb",
            Preset::MapONT => "map-ont",
            Preset::MapPB => "map-pb",
            Preset::Splice => "splice",
            Preset::None => "",
        };
        CString::new(preset).unwrap()
    }
}

/// Construct presets from a given preset. Currently, the presets used by minimap2 command line application
/// are supported.
/// # Example
/// ```
/// # use minimap2_rust::*;
/// let (indexopt,mapopt) = default_preset(&Preset::MapPB).unwrap();
/// ```
pub fn default_preset(preset: &Preset) -> Option<(IndexOption, MappingOption)> {
    if preset == &Preset::None {
        preset_none()
    } else {
        let (mut idx, mut map) = preset_none()?;
        let preset = preset.to_cstr();
        let res = unsafe { mm_set_opt(preset.as_ptr(), &mut idx.inner, &mut map.inner) };
        if res == 0 {
            Some((idx, map))
        } else {
            None
        }
    }
}

// preset of none. default.
fn preset_none() -> Option<(IndexOption, MappingOption)> {
    let (res, index_option, mapping_option) = unsafe {
        let mut index_option: mm_idxopt_t = std::mem::uninitialized();
        let mut mapping_option: mm_mapopt_t = std::mem::uninitialized();
        let res = mm_set_opt(std::ptr::null(), &mut index_option, &mut mapping_option);
        (res, index_option, mapping_option)
    };
    if res == 0 {
        let index_option = IndexOption {
            inner: index_option,
        };
        let mapping_option = MappingOption {
            inner: mapping_option,
        };
        Some((index_option, mapping_option))
    } else {
        None
    }
}

pub fn check_option(idx: &IndexOption, map: &MappingOption) -> bool {
    unsafe {
        if mm_check_opt(&idx.inner, &map.inner) == 0 {
            true
        } else {
            false
        }
    }
}

/// Determine whether or not the given file is a valid index file.
/// Return Some(len), when the given file is len-length index file,
/// None otherwise.
pub fn is_index(file_name: &str) -> Option<usize> {
    let size = unsafe { mm_idx_is_idx(CString::new(file_name).unwrap().as_ptr()) };
    if size == 0 {
        None
    } else {
        Some(size as usize)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
