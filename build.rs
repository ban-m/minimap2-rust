extern crate bindgen;
use std::env;
use std::path::PathBuf;
fn main(){
    println!("cargo:rustc-link-lib=minimap2");
    println!("cargo:rustc-link-lib=z");
    println!("cargo:rustc-link-search=/home/ban-m/local/lib/");
    let bindings = bindgen::Builder::default()
        .header("wrapper.h")
        .generate()
        .expect("Error");
    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings!");
}
