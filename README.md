# Minimap2 from Rust


Author: Bansho Masutani ban-m@g.ecc.u-tokyo.ac.jp

# Description

This is a wrapper library to use `minimap2` from Rust programming language.

## How to use

1. First, clone this repository to the directory:
```bash
git clone git@bitbucket.org:ban-m/minimap2-rust.git
```

2. Write a dependency to the `Cargo.toml` in your project as follows
```
[dependencies]
minimap2-rust  = { paht = "/to/the/cloned/directory/"}
```

3. Import crate, use it!
```rust
extern crate minimap2_rust;
fn main(){
   let op = minimap2_rust::default_preset(&minimap2_rust::Preset::MapPB).unwrap()
   // continue...
}
```

To read the documentation, generate document by `cargo doc`.

## License
MIT